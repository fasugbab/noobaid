from django.urls import path, include
from django.contrib.auth import views as auth_views

from default.forms import  UserForm
from . import views

app_name='default'
urlpatterns=[
    path('',views.index,name='index'),
    path('jobs',views.job_list,name='job-list'),
    path('jobs/<int:job_id>',views.job_detail,name='job-detail'),
    path('jobs/<int:job_id>/apply',views.job_apply,name='job-apply'),
    path('jobs/success',views.success,name='success'),
    path('accounts/login',views.login_user,name='login'),
    path('accounts/logout',views.logout_user,name='log-out'),
    path('accounts/sign_up',views.signup,name='sign-up'),
    path('send',views.email,name='up'),
]