from django import forms
from django.contrib.auth.models import User

from default.models import Profile, JobApplication


class UserForm(forms.ModelForm):
    class Meta:
        model=User
        fields=['username','password','email','first_name','last_name']
        widgets={
            'username':forms.EmailInput(attrs={'class':'form-control','placeholder':'email','noautocomplete':''}),
            'first_name':forms.EmailInput(attrs={'class':'form-control','placeholder':'First Name','noautocomplete':''}),
            'last_name':forms.EmailInput(attrs={'class':'form-control','placeholder':'LastName','noautocomplete':''}),
            'password' : forms.PasswordInput(attrs={'class':'form-control','placeholder':'Password (6 - 15 Characters)'}),
        }

class ProfileForm(forms.ModelForm):
    class Meta:
        model=Profile
        fields=['instituition']
        widgets={

            # 'username':forms.EmailInput(attrs={'class':'form-control','placeholder':'email','noautocomplete':''}),
            # 'password' : forms.PasswordInput(attrs={'class':'form-control','placeholder':'password'}),
            # 'email' : forms.TextInput(attrs={'class':'form-control','placeholder':'email'}),
            'instituition' : forms.TextInput(attrs={'class':'form-control','placeholder':'instituition'})


        }

class JobApplicationForm(forms.ModelForm):
    class Meta:
        model=JobApplication
        exclude=['updated_on']
        widgets={
            'resume':forms.FileInput(attrs={'class':'input file'})
        }