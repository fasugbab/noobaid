
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
# Create your views here.
from django.template.response import TemplateResponse

from default.forms import UserForm, ProfileForm, JobApplicationForm
from default.models import Profile, Job, JobApplication
from noobaid import settings

global next_page
def index(request):
    return TemplateResponse(request,'default/index.html')


def login_user(request):
    if request.GET:
        global next_page
        next_page=request.GET.get('next')
        print('This is next column')

    error_message = ""
    if request.user.is_authenticated:
        return redirect("default:index")
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        try:
            user = User.objects.get(username=username)
            if not user.is_active:
                error_message="Sorry, account disabled, contact the administrator"
            if not error_message:
                if check_password(password, user.password):
                    a=authenticate(username=username,password=password)
                    login(request,user)
                else:
                    error_message = "Invalid Password"
        except User.DoesNotExist:
            error_message = "Sorry, No account exists with that username"

        finally:
            if error_message:
                return TemplateResponse(request, "registration/login.html", {"error_message": error_message})
            if next_page:
                return HttpResponseRedirect(next_page)
            else:
                return redirect("default:index")
    else:
        return TemplateResponse(request, "registration/login.html", {"error_message": error_message})


def logout_user(request):
    logout(request)
    return redirect("default:login")



def signup(request):
    _form = UserForm(request.POST or None)
    _pform = ProfileForm(request.POST or None)
    if request.method == "POST":
        user = User()
        profile = Profile()
        form = UserForm(request.POST, instance=user)
        pform = ProfileForm(request.POST, instance=profile)
        if form.is_valid() and pform.is_valid():
            new_user = form.save(commit=False)
            new_user.email = form.cleaned_data['username']
            new_user.save()
            new_user.set_password(user.password)
            new_user.save()
            p= Profile.objects.get(user=new_user)
            p.instituition=pform.cleaned_data['instituition']
            p.save()
            logout(request)
            return TemplateResponse(request,'registration/login.html',{'error_message':'Account Created, log in here'})
        else:
            return TemplateResponse(request, 'registration/signup.html', {"userform": _form,'profileform':_pform})
    else:
        return TemplateResponse(request, 'registration/signup.html', {"userform": _form,'profileform':_pform})

def job_list(request):
    job_list = Job.objects.all()
    page = request.GET.get('page', 1)

    paginator = Paginator(job_list, 10)
    try:
        jobs = paginator.page(page)
    except PageNotAnInteger:
        jobs = paginator.page(1)
    except EmptyPage:
        jobs = paginator.page(paginator.num_pages)

    return TemplateResponse(request, 'default/joblist.html', {'jobs': jobs})

@login_required
def job_detail(request,job_id):
    context={}
    try:
        job=Job.objects.get(pk=job_id)
    except Job.DoesNotExist:

        return TemplateResponse(request,'default/page404.html')
    context.update({'job':job})



    return TemplateResponse(request,'default/job_detail.html',context)

@login_required
def job_apply(request,job_id):
    job=get_object_or_404(Job,pk=job_id)
    form=JobApplicationForm(request.POST or None,request.FILES or None)

    if request.method=='POST':
        if form.is_valid():
            f=form.save(commit=False)
            f.job=job
            f.user=get_object_or_404(User,username=request.user.username)
            f.save()
            return redirect('default:success')
        else:
            return TemplateResponse(request,'default/job_apply.html',{'job':job,'form':form})
    else:
        return TemplateResponse(request,'default/job_apply.html',{'job':job,'form':form})


def success(request):
    return TemplateResponse(request,'default/success.html')

def email(request):
    subject = 'Thank you for registering to our site'
    message = ' it  means a world to us '
    recipient_list = ['azmayowa@gmail.com',]
    send_mail(subject=subject, message=message, recipient_list=recipient_list )
    return HttpResponse('sent')