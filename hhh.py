from enum import Enum
import random


class PASSWORD_TYPE(Enum):
    STRONG = 2
    WEAK = 3


# Simple Password Generator
def generate(type, length=8):
    strong_password_key = "ASDFGHJK!@#$%^&*?><,.{}:~|1234567|\LPOIabcd12343UYTREWQZXCVBNM"
    weak_password_key = "ASDFGHJKLPOIUYTREWQZXCVBNM"
    if type == PASSWORD_TYPE.STRONG:
        print('\n Generating Strong Password.\n')
        passw = "".join(random.sample(strong_password_key, length))
        return passw
    else:
        print('\n Generating Weak Password.\n')
        passw = "".join(random.sample(weak_password_key, length))
        return passw


def init():
    print("Simple Python Password Generator ")
    print("")
    password_type = int(input("Select \n 2. Strong Password \n 3. Weak Password\n"))
    if password_type not in range(0, 2):
        password_length = input("Enter length (0-14)")
        if password_length not in range(0, 2):
            print('Generated Password = %s ' % generate(password_type, int(password_length)))

    else:
        print('Wrong Input')
        init()


init()
input()
